

class Config:

    DEBUG = True
    INDEX_NEWS = 'news'
    INDEX_RECIPES = 'recipes'
    INDEX_PRODUCTS = 'products'

    INDEX_WORDS = {
        INDEX_NEWS: ['дерев', 'садов', 'кольц', 'добр', 'автобус',
                     'выставк', 'it', 'технолог'],
        INDEX_RECIPES: ['рецепт', 'борщ', 'яблочн', 'пирог', 'тайск', 'кухн'],
        INDEX_PRODUCTS: ['дет', 'капита', 'грант', 'зимн', 'шин', 'тайск',
                         'кухн']
    }

    ELASTIC_URI = 'http://127.0.0.1:9200/'
