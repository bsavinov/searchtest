import os
import json
from elasticsearch import Elasticsearch
from flask import Flask

dir_path = os.path.dirname(os.path.realpath(__file__))
app = Flask(__name__)
app.config.from_object('config.Config')
es = Elasticsearch([app.config['ELASTIC_URI']])
indicies = [app.config['INDEX_NEWS'], app.config['INDEX_RECIPES'],
            app.config['INDEX_PRODUCTS']]


def create_indexes():
    for idx in indicies:
        f_name = '{0}.json'.format(idx)
        f_path = os.path.join(dir_path, 'deploy', 'schemas', f_name)
        if os.path.exists(f_path):
            with open(f_path) as f:
                mapping = f.read()
                es.indices.create(index=idx, body=mapping)
                print('Index "{0}" created'.format(idx))


def fill_indexes():
    f_path = os.path.join(dir_path, 'deploy', 'test-documents.json')
    if not os.path.exists(f_path):
        print('ERROR: path "{0}" does not exist'.format(f_path))
        return
    data = json.loads(open(f_path).read())
    for entry in data:
        idx = entry['index']
        doc = {'title': entry['title'], 'body': entry['body']}
        es.index(index=idx, doc_type='entry', body=doc)

if __name__ == '__main__':
    create_indexes()
    fill_indexes()
