from operator import itemgetter
from nltk.stem import SnowballStemmer
from flask import Flask, render_template, request, jsonify
from elasticsearch import Elasticsearch

app = Flask(__name__)
app.config.from_object('config.Config')
es = Elasticsearch([app.config['ELASTIC_URI']])


@app.route('/')
def index_page():
    return render_template('index.html')


@app.route('/search/', methods=['GET', 'POST'])
def search():
    query = request.args.get('query')
    indexes = get_indexes_by_query(query)
    search_results = []
    s_result = {'error': False, 'error_message': '', 'response': ''}
    if not indexes:
        s_result = {'error': True,
                    'error_message': 'No indexes found for query'}
    else:
        for idx in indexes:
            q = {
                "query": {
                    "bool": {
                        "should": [
                            {
                                "multi_match": {
                                    "query": query,
                                    "fields": ["title^2", "body"],
                                    "type": "cross_fields"
                                }
                            }
                        ]
                    }
                },
                "min_score": 0.5,
                "size": 1
            }
            r = es.search(index=idx, body=q)
            if not len(r['hits']['hits']):
                continue
            title = r['hits']['hits'][0]['_source']['title']
            body = r['hits']['hits'][0]['_source']['body']
            score = r['hits']['hits'][0]['_score']
            doc = {'title': title, 'body': body, 'score': score}
            search_results.append(doc)
        if len(search_results):
            search_results = sorted(search_results, key=itemgetter('score'),
                                    reverse=True)
            s_result['response'] = search_results
    return jsonify(s_result)


def get_indexes_by_query(query):
    stemmed_query = stem_query(query)
    s_query = set(stemmed_query)

    index_words = app.config['INDEX_WORDS']
    indexes = []
    for idx in index_words.keys():
        num_coincidences = len(set(index_words[idx]) & s_query)
        if num_coincidences > 0:
            indexes.append(idx)
    return indexes


def stem_query(query):
    stm_ru = SnowballStemmer('russian')
    result = []
    query = query.replace('/', ' ').replace('\\', ' ') \
        .replace('"', '').replace("'", '').replace('+', ' ') \
        .replace('(', '').replace(')', '').replace('  ', ' ')\
        .replace('-', ' ')
    query = query.lower()
    values = query.split(' ')
    for v in values:
        result.append(stm_ru.stem(v))
    return result

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=9999)
